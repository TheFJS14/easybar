module.exports = {
    PORT: process.env.PORT,
    DB: {
      user: "postgres",
      password: "mysecretpassword",
      database: "easybar_QA",
      host: "localhost",
      dialect: "postgres"
    }
  };
  