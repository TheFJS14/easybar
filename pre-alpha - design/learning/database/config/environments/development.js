module.exports = {
    PORT: process.env.PORT,
    DB: {
      user: "postgres",
      password: "mysecretpassword",
      database: "easybar_dev",
      host: "localhost",
      dialect: "postgres"
    }
  };
  