module.exports = {
  PORT: process.env.PORT,
  DB: {
    user: "postgres",
    password: "mysecretpassword",
    database: "easybar_prod",
    host: "localhost",
    dialect: "postgres"
  }
};
