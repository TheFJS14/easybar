let mongoose = require('mongoose'),
  express = require('express'),
  router = express.Router();

//Modelo de Student
let studentSchema = require('../models/Student');

//Create Student
router.route('/create-student').post((req, res, next) => {
  studentSchema.create(req.body, (error, data) => {
    if (error) {
      return next(error);
    } else {
      console.log(data);
      res.json(data);
    }
  });
});

//Read students
router.route('/').get((req, res) => {
  studentSchema.find((error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

//Get single student
router.route('/update-student/:id').put((req, res, next) => {
  studentSchema.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    (error, data) => {
      if (error) {
        return next(error);
        console.log(error);
      } else {
        res.json(data);
        console.log('Student updated successfully!');
      }
    }
  );
});

//Delete student
router.route('/delete-student/:id').put((req, res, next) => {
  studentSchema.findByIdAndUpdate(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({ msg: data });
    }
  });
});
