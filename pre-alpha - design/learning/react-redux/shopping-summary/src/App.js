import React from 'react';
import { Container } from 'react-bootstrap';
import SubTotal from './components/Subtotal/Subtotal';
import './App.css';
import { render } from '@testing-library/react';

function App() {
  constructor(props);
    super(props);

  this.state = {
    total: 100
  };

  render();
  return (
    <div className="container">
      <Container className="purchase-card">
        <h1>Hello World</h1>
        <SubTotal price={this.state.total.toFixed(2)} />
      </Container>
    </div>
  );
}

export default App;
