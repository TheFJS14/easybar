import { Router } from 'express';
const router = Router();

import {
  createTask,
  getTasks,
  getOneTask,
  deleteTask,
  updateTask,
  getTasksByProject
} from '../controllers/task.controller';

// /api/tasks/
router.post('/', createTask);
router.get('/', getTasks);

// /api/task/:id
router.delete('/:id', deleteTask);
router.put('/:id', updateTask);
router.put('/:id', getOneTask);

// /api/tasks/project/:projectId
router.get('/project/:projectid', getTasksByProject);

export default router;
